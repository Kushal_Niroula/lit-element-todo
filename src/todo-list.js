import { html, css, LitElement } from "lit";

import { classMap } from "lit-html/directives/class-map.js";

import "@polymer/iron-icon/iron-icon.js";
import "@polymer/iron-icons/iron-icons.js";

/**
 * TodoList component class
 */
export class TodoList extends LitElement {
  /**
   * Runs after the object is initialized
   */
  constructor() {
    super();

    this.isUpdating = false;
    this.deleteTodo = () => {};
    this.updateTodo = () => {};
    this.markComplete = () => {};
    this.triggerEdit = () => {};
    this.closeEdit = () => {};
    this.handleActive = () => {};
    this.handleInactive = () => {};
  }

  static get styles() {
    return css`
      .strike-through {
        text-decoration: line-through;
      }
      .todo-element {
        margin-left: 10px;
        margin-top: 10px;
      }
      .todo-text {
        margin-left: 3px;
      }
      .delete-button {
        display: none;
      }
      .active {
        display: inline;
      }
      iron-icon {
        cursor: pointer;
      }
      #todoText {
        width: 250px;
        margin: 8px 0;
        display: inline-block;
      }
    `;
  }

  /**
   * Defines properties of the component
   */
  static get properties() {
    return {
      /**
       * List of todo objects
       */
      todos: { type: Array },
      /**
       * Flag to check if any todos are currently being edited
       */
      isUpdating: { type: Boolean },
      /**
       * Text being updated
       */
      updateText: { type: String },
      /**
       * Function to delete todo
       */
      deleteTodo: { type: Function },
      /**
       * Function to update todo
       */
      updateTodo: { type: Function },
      /**
       * functionn to mark todo task as completed
       */
      markComplete: { type: Function },
      /**
       * function to trigger edit input box
       */
      triggerEdit: { type: Function },
      /**
       * function to close edit input box
       */
      closeEdit: { type: Function },
      /**
       * Function to handle active state on hover
       */
      handleActive: { type: Function },
      /**
       * Function to handle inactive state on mouseleave
       */
      handleInactive: { type: Function },
    };
  }

  /**
   * Handles updating the todo dispatches cutom event to update task
   * @param {Number} index
   */
  handleUpdate(index) {
    if (this.updateText) {
      this.updateTodo(index, this.updateText);
      this.isUpdating = false;
      this.updateText = "";
    }
  }

  /**
   * controlled input for updateText
   * @param {Event} event
   */
  handleUpdateText(event) {
    this.updateText = event.target.value.trim();
  }

  /**
   * Return todo-text as per the edit/completed state
   * @param {Object} todo
   * @returns {HTML}
   */
  getTodoText(todo) {
    const textClass = {
      "todo-text": true,
      "strike-through": todo.completed,
    };

    if (todo.edit) {
      return html`<paper-input
        type="text"
        @input=${this.handleUpdateText}
        .value=${this.updateText}
        id="todoText"
      ></paper-input>`;
    }
    return html` <paper-textarea class=${classMap(textClass)}>
      ${todo.text}
    </paper-textarea>`;
  }

  /**
   * Renders component
   * @returns Html
   */
  render() {
    return html` <div id="todos">
      ${this.todos.map((todoLi, index) => {
        const buttonClass = {
          active: todoLi.edit
            ? true
            : this.isUpdating
            ? false
            : todoLi.active
            ? true
            : false,
        };
        return html`<div
          class="todo-element"
          @mouseover=${() => this.handleActive(index)}
          @mouseout=${() => this.handleInactive(index)}
        >
          <input
            type="checkbox"
            class="action-button"
            .checked=${todoLi.completed}
            @click=${() => this.markComplete(index)}
          />

          ${this.getTodoText(todoLi)}

          <span class="delete-button ${classMap(buttonClass)}">
            ${this.isUpdating
              ? todoLi.edit
                ? html`<iron-icon
                      icon="icons:check"
                      @click=${() => {
                        this.isUpdating = false;
                        this.handleUpdate(index);
                        this.closeEdit(index);
                      }}
                    ></iron-icon>
                    <iron-icon
                      icon="icons:clear"
                      @click=${() => {
                        this.isUpdating = false;
                        this.closeEdit(index);
                        this.updateText = "";
                      }}
                    ></iron-icon>`
                : html`a`
              : html` <iron-icon
                    icon="icons:delete"
                    @click=${() => this.deleteTodo(index)}
                  ></iron-icon>
                  <iron-icon
                    icon="icons:create"
                    @click=${() => {
                      this.isUpdating = true;
                      this.triggerEdit(index);
                      this.updateText = todoLi.text;
                    }}
                  ></iron-icon>`}
          </span>
        </div>`;
      })}
    </div>`;
  }
}

window.customElements.define("todo-list", TodoList);
