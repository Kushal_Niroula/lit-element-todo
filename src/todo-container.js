import { html, css, LitElement } from "lit";

import "@polymer/iron-icon/iron-icon.js";
import "@polymer/iron-icons/iron-icons.js";
import "@polymer/paper-input/paper-input.js";
import "@polymer/paper-button/paper-button.js";

import "./todo-list";
import "./todo-header";

/**
 * TodoContainer component class
 */
export class TodoContainer extends LitElement {
  /**
   * Styles the component
   * @returns {CSS}
   */
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--todo-list-text-color, #000);
      }
      #body {
        margin-top: 50px;
      }
    `;
  }

  /**
   * Defines properties of the component
   */
  static get properties() {
    return {
      /**
       * Array of todos
       */
      todos: { type: Array },
    };
  }
  /**
   * Runs after the object is initialized
   */
  constructor() {
    super();
    this.todos = [];
    this.handleDelete = this.handleDelete.bind(this);
    this.updateTodo = this.updateTodo.bind(this);
    this.markComplete = this.markComplete.bind(this);
    this.openEdit = this.openEdit.bind(this);
    this.closeEdit = this.closeEdit.bind(this);
    this.handleActive = this.handleActive.bind(this);
    this.handleInactive = this.handleInactive.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Adds todos in the todo-list
   * @param {Event} event
   */
  handleSubmit(text) {
    if (text) {
      this.todos = [
        ...this.todos,
        { text, completed: false, active: false, edit: false },
      ];
    }
  }

  /**
   *
   * marks the todo task as completed
   * @param {Event} event
   */
  markComplete(id) {
    this.todos = this.todos.map((todo, index) => {
      if (index === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
  }

  /**
   *
   * Deletes the task
   * @param {Event} event
   */
  handleDelete(id) {
    this.todos = this.todos.filter((todo, index) => {
      if (index !== id) {
        return todo;
      }
    });
  }

  /**
   * Updates todo
   * @param {Event} event
   */
  updateTodo(id, text) {
    this.todos = this.todos.map((todo, index) => {
      if (index === id) {
        return { ...todo, text: text };
      } else {
        return todo;
      }
    });
  }

  /**
   * opens edit input box for particular task
   * @param {Number} index
   */
  openEdit(id) {
    this.todos = this.todos.map((todo, index) => {
      if (index === id) {
        return { ...todo, edit: true };
      }
      return todo;
    });
  }

  /**
   * opens edit input box for particular task
   * @param {Number} index
   */
  closeEdit(id) {
    this.todos = this.todos.map((todo, index) => {
      if (index === id) {
        return { ...todo, edit: false };
      }
      return todo;
    });
  }

  /**
   *
   * marks currently hovered task as active
   * @param {Number} index
   */
  handleActive(id) {
    this.todos = this.todos.map((todo, index) => {
      if (index === id) {
        return { ...todo, active: true };
      }
      return todo;
    });
  }

  /**
   *
   * marks task as inactive when cursor leaves
   * @param {Number} index
   */
  handleInactive(id) {
    this.todos = this.todos.map((todo, index) => {
      if (index === id) {
        return { ...todo, active: false };
      }
      return todo;
    });
  }

  /**
   * Renders component
   * @returns {HTML}
   */
  render() {
    return html`
      <div id="body">
        <todo-header .addTodo=${this.handleSubmit}></todo-header>
        <todo-list
          .deleteTodo=${this.handleDelete}
          .todos=${this.todos}
          .updateTodo=${this.updateTodo}
          .markComplete=${this.markComplete}
          .triggerEdit=${this.openEdit}
          .closeEdit=${this.closeEdit}
          .handleActive=${this.handleActive}
          .handleInactive=${this.handleInactive}
        ></todo-list>
      </div>
    `;
  }
}

window.customElements.define("todo-container", TodoContainer);
