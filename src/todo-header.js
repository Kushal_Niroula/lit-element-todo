import { html, css, LitElement } from "lit";

/**
 * TodoHeader component class
 */
export class TodoHeader extends LitElement {
  /**
   * Runs after the object is initialized
   */
  constructor() {
    super();

    this.inputText = "";
  }

  /**
   * @returns {css}
   */
  static get styles() {
    return css`
      #todoText {
        width: 250px;
        margin: 8px 0;
        display: inline-block;
      }
      #addTodo {
        background-color: #4caf50;
        border: none;
        color: white;
        padding: 12px 28px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
      }
    `;
  }

  /**
   * Defines properties of the component
   */
  static get properties() {
    return {
      /**
       * Controlled input text to add todos
       */
      inputText: { type: String },
      /**
       * Function to add todos
       */
      addTodo: { type: Function },
    };
  }
  /**
   * dispatches custom event to add todos
   */
  handleAdd() {
    if (this.inputText) {
      this.addTodo(this.inputText);
      this.inputText = "";
    }
  }

  /**
   * Controlled input for todoText
   * @param {Event} event
   */
  handleChange(event) {
    event.stopPropagation();
    this.inputText = event.target.value.trim();
  }

  /**
   * Renders component
   * @returns {HTML}
   */
  render() {
    return html` <paper-input
        type="text"
        label="What's on your mind"
        @input=${this.handleChange}
        .value=${this.inputText}
        id="todoText"
      ></paper-input>
      <paper-button raised @click=${this.handleAdd} id="addTodo"
        >Add</paper-button
      >`;
  }
}

window.customElements.define("todo-header", TodoHeader);
